# flutter unity

A Flutter plugin for embedding Unity projects in Flutter projects.

Both Android and iOS are supported.


## Configuring your Unity project
#### Android
1. Go to **File** > **Build Settings...** to open the [Build Settings](https://docs.unity3d.com/Manual/BuildSettings.html) window.
2. Select **Android** and click **Switch Platform**.
3. Click **Add Open Scenes**.
4. Check **Export Project**.
5. Click **Player Settings...** to open the [Player Settings](https://docs.unity3d.com/Manual/class-PlayerSettings.html) window.
6. In the [Player Settings](https://docs.unity3d.com/Manual/class-PlayerSettings.html) window, configure the following:

Resolution and Presentation > Start in fullscreen mode > NO
Other Settings > Rendering > Graphics API > Add OpenGLES3
Other Settings > Configuration > Scripting Backend > IL2CPP
Other Settings > Configuration > Target Architectures > ARMv7, ARM64


7. Close the [Player Settings](https://docs.unity3d.com/Manual/class-PlayerSettings.html) window.
8. Click **Export** and save as `unityExport`.
#### iOS
1. Go to **File** > **Build Settings...** to open the [Build Settings](https://docs.unity3d.com/Manual/BuildSettings.html) window.
2. Select **iOS** and click **Switch Platform**.
3. Click **Add Open Scenes**.
4. Click **Build** and save as `UnityProject`.

## Configuring your Flutter project
#### Android
1. Copy the `unityExport` folder to `<your_flutter_project>/android/unityExport`.
2. Run `flutter pub run flutter_unity:unity_export_transmogrify`.
3. Open `<your_flutter_project>/android/unityExport/build.gradle` and check if `buildTypes { profile {} }` is present. If not, add the following:
```
buildTypes {
    profile {}
}
```
Refer to the [project's unityExport/build.gradle]

4. Open `<your_flutter_project>/android/build.gradle` and, under `allprojects { repositories {} }`, add the following:
```
flatDir {
    dirs "${project(':unityExport').projectDir}/libs"
}
```
Refer to the [project's build.gradle]

5. Open `<your_flutter_project>/android/settings.gradle` and add the following:
```
include ':unityExport'
```
Refer to the [project's settings.gradle]

6. Open `<your_flutter_project>/android/app/src/main/AndroidManifest.xml` and add the following:
```
<uses-permission android:name="android.permission.WAKE_LOCK"/>
```
Refer to the [project's AndroidManifest.xml]

Steps 1, 2 and 3 must be repeated for every new build of the Unity project.

#### iOS
1. Copy the `UnityProject` folder to `<your_flutter_project>/ios/UnityProject` and open `<your_flutter_project>/ios/Runner.xcworkspace` in **Xcode**.
2. Go to **File** > **Add Files to "Runner"...**, and add `<your_flutter_project>/ios/UnityProject/Unity-iPhone.xcodeproj`.
3. Select `Unity-iPhone/Data`, and, in the **Inspectors** pane, set the **Target Membership** to **UnityFramework**.
4. Select `Unity-iPhone`, select **PROJECT** : **Unity-iPhone**, and, in the **Build Settings** tab, configure the following:

Build Options > Enable Bitcode > No
Linking > Other Linker Flags > -Wl,-U,_FlutterUnityPluginOnMessage


5. Select `Runner`, select **TARGETS** : **Runner**, and, in the **General** tab, configure the following:
Frameworks, Libraries, and Embedded Content > Name , Embed
UnityFramework.framework > Embed & Sign


6. Select `Runner/Runner/Info.plist`, and configure the following:
io.flutter.embedded_views_preview > Boolean Yes



Steps 1, 3 and 4 must be repeated for every new build of the Unity project.

## Exchanging messages between Flutter and Unity

#### Flutter

 - In this Project we have used TCP-IP connection for exchanging messages between Flutter and Unity.
  
Refert to lib/main.dart for flutter side TCP-IP configuration.

    hostname = '127.0.0.1'; // Binds to all adapters, Host name should be 127.0.0.1 it's won't work with localhost string.
    port = 8213;

   listenServer() => This function first make the server at 127.0.0.1:8213 ip address and then it will listen for new connection and messages form clients.

   soc.writeln("start"); => by this function you can send messages to the unity.

#### Unity

Refer to /unity/FlutterUnityExample/Assets/Rotate.cs file for unity side connection.


## Add this dependencies to your file.

   using System.Net.Sockets;
   using System.Text;
   using System.Threading;
 
- Add ConnectToTcpServer()  this funcion to your on  Start() function. it will connection flutter tcp server on Start of unity app.
- By SendMessage() this function you can send messages to the server.