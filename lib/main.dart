import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_unity/flutter_unity.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  static const hostname = 'localhost'; // Binds to all adapters
  static const port = 8052;
  ServerSocket server;
  Socket socket;

  
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Face Changer Demo'),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => UnityViewPage(
                          server: server,
                        )));
          },
          child: Text('Test'),
        ),
      ),
    );
  }
}

class UnityViewPage extends StatefulWidget {
  ServerSocket server;
  UnityViewPage({Key key, this.server}) : super(key: key);
  @override
  _UnityViewPageState createState() => _UnityViewPageState();
}

class _UnityViewPageState extends State<UnityViewPage> {
  UnityViewController unityViewController;
    static const hostname = '127.0.0.1'; // Binds to all adapters
  static const port = 8213;
  ServerSocket server;
  Socket soc;
  @override
  void initState() {
    listenServer();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  listenServer() async {
        server = await ServerSocket.bind(hostname, port);
    print('TCP server started at ${server.address}:${server.port}.');

    try {
      server.listen((Socket socket) {
        print('New TCP client ${socket.address.address}:${socket.port} connected.');
        soc = socket;
        soc.writeln("Helloooooooooo"); 
        socket.listen((Uint8List data) {
          if (data.length > 0 && data.first == 10) return;
          final msg = data.toString();
          print('Data from client: $msg');
        
        }, onError: (error) {
          print('Error for client ${socket.address.address}:${socket.port}.');
        }, onDone: () {
          print(
              'Connection to client ${socket.address.address}:${socket.port} done.');
        });
      });
    } on SocketException catch (ex) {
      print(ex.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Face Changer Demo'),
        ),
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: UnityView(
                onCreated: onUnityViewCreated,
                onReattached: onUnityViewReattached,
                onMessage: onUnityViewMessage,
              ),
            ),
            Positioned(
              top: 15,
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    RaisedButton(
                      color: Colors.black,
                      onPressed: () {
                      soc.writeln("start");
                      },
                      child: Text('Start',style: TextStyle(color: Colors.white),),
                    ),
                    SizedBox(
                      width: 100,
                    ),
                    RaisedButton(
                      color: Colors.black,
                      onPressed: () {
                      soc.writeln("stop");
                      },
                      child: Text('Stop',style: TextStyle(color: Colors.white),),
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }

  void onUnityViewCreated(UnityViewController controller) {
    print('onUnityViewCreated');

    unityViewController = controller;

    controller.send(
      'Cube',
      'SetRotationSpeed',
      '30',
    );
  }

  void onUnityViewReattached(UnityViewController controller) {
    print('onUnityViewReattached');
  }

  void onUnityViewMessage(UnityViewController controller, String message) {
    print('onUnityViewMessage');
    log(":::::: message from unity $message.");
  }
}
