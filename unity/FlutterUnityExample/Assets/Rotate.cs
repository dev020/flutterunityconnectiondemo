﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;


public class Rotate : MonoBehaviour
{
    
    [SerializeField]
    private Vector3 v3;
	private TcpClient socketConnection; 	
	private Thread clientReceiveThread; 
    // Start is called before the first frame update
    void Start()
    {
        	ConnectToTcpServer();     
        v3 = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(v3 * Time.deltaTime * 10);
    }

    public void SetRotationSpeed(string data)
    {
        FlutterUnityPlugin.Message message = FlutterUnityPlugin.Messages.Receive(data);

        float value = float.Parse(message.data);

        v3 = new Vector3(value, value, value);

        message.data = "SetRotationSpeed: " + value;

        FlutterUnityPlugin.Messages.Send(message);
    }

    private void ConnectToTcpServer () { 	
        Debug.Log("ConnectToTcpServer called"); 			
		try {  			
			clientReceiveThread = new Thread (new ThreadStart(ListenForData)); 			
			clientReceiveThread.IsBackground = true; 			
			clientReceiveThread.Start();  		
		} 		
		catch (Exception e) { 			
			Debug.Log("On client connect exception " + e); 		
		} 	
	}  	
	/// <summary> 	
	/// Runs in background clientReceiveThread; Listens for incomming data. 	
	/// </summary>     
	private void ListenForData() { 		
		try { 			
			socketConnection = new TcpClient("127.0.0.1", 8213);  			
			Byte[] bytes = new Byte[1024];             
			while (true) { 				
				// Get a stream object for reading 				
				using (NetworkStream stream = socketConnection.GetStream()) { 					
					int length; 					
					// Read incomming stream into byte arrary. 					
					while ((length = stream.Read(bytes, 0, bytes.Length)) != 0) { 						
						var incommingData = new byte[length]; 						
						Array.Copy(bytes, 0, incommingData, 0, length); 						
						// Convert byte array to string message. 						
						string serverMessage = Encoding.ASCII.GetString(incommingData); 						
						Debug.Log("server message received as: " + serverMessage); 					
					} 				
				} 			
			}         
		}         
		catch (SocketException socketException) {             
			Debug.Log("Socket exception: " + socketException);         
		}     
	}  	
	/// <summary> 	
	/// Send message to server using socket connection. 	
	/// </summary> 	
	private void SendMessage() {         
		if (socketConnection == null) {             
			return;         
		}  		
		try { 			
			// Get a stream object for writing. 			
			NetworkStream stream = socketConnection.GetStream(); 			
			if (stream.CanWrite) {
				var data = @"{
	""id"": ""0001"",
	""type"": ""donut"",
	""name"": ""Cake"",
	""ppu"": 0.55,
	""sweepIndex"":0,
	""totalRadials"":720,
	""beamWidth"":0.949999988079071,
	""startingUnixtime"":1536863321000,
	""endingUnixtime"":1536863574000,
	""totalGatesPerRay"":1832,
	""gateDepthMeters"":250.0,
	""distanceToFirstGateMeters"":2125.0,
	""meanElevationDeg"":0.5275726318359375,
	""originLatitude"":33.989444444444445,
	""originLongitude"":-78.42888888888889,
	""originAltitude"":20.0,
	""deviantOriginCount"":0,
	""batters"" : {
			""batter"":
				[
					{ ""id"": ""1001"", ""type"": ""Regular"" },
					{ ""id"" : ""1002"", ""type"": ""Chocolate"" },
					{ ""id"": ""1003"", ""type"": ""Blueberry"" },
					{ ""id"": ""1004"", ""type"": ""Devil's Food"" }
					{ ""id"": ""1005"", ""type"": ""Regular"" },
					{ ""id"" : ""1006"", ""type"": ""Chocolate"" },
					{ ""id"": ""1007"", ""type"": ""Blueberry"" },
					{ ""id"": ""1008"", ""type"": ""Devil's Food"" }
					{ ""id"": ""1009"", ""type"": ""Regular"" },
					{ ""id"" : ""1010"", ""type"": ""Chocolate"" },
					{ ""id"": ""1011"", ""type"": ""Blueberry"" },
					{ ""id"": ""1012"", ""type"": ""Devil's Food"" }
				]
		},
	""topping"":
		[
				    { ""id"": ""5001"", ""type"": ""None"" },
					{ ""id"" : ""5002"", ""type"": ""Glazed"" },
					{ ""id"": ""5003"", ""type"": ""Sugar"" },
					{ ""id"": ""5004"", ""type"": ""Powdered Sugar"" }
		            { ""id"": ""5005"", ""type"": ""Chocolate with Sprinkles"" },
					{ ""id"" : ""5006"", ""type"": ""Chocolate"" },
					{ ""id"": ""5007"", ""type"": ""Maple"" },
					{ ""id"": ""5008"", ""type"": ""Powdered Sugar"" }
					{ ""id"": ""5009"", ""type"": ""None"" },
					{ ""id"" : ""5010"", ""type"": ""Glazed"" },
					{ ""id"": ""5011"", ""type"": ""Sugar"" },
					{ ""id"": ""5012"", ""type"": ""Powdered Sugar"" }
		            { ""id"": ""5013"", ""type"": ""Chocolate with Sprinkles"" },
		]
}";
				string clientMessage = data; 				
				// Convert string message to byte array.                 
				byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(clientMessage); 				
				// Write byte array to socketConnection stream.                 
				stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);                 
				Debug.Log("Client sent his message - should be received by server");             
			}         
		} 		
		catch (SocketException socketException) {             
			Debug.Log("Socket exception: " + socketException);         
		}     
	} 

  public void onSendMessageButtonClick() {
    SendMessage();
  }

}
